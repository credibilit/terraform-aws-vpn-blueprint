// VPC
resource "aws_vpc" "test" {
  cidr_block = "10.0.0.0/16"
  tags = {
    Name = "acme-vpn-test"
  }
}

// VPN
module "test" {
  source  = "../"
  account = "${var.account}"

  vpn_name                    = "vpc-acme"
  customer_name               = "customer-acme"
  vpc_id                      = "${aws_vpc.test.id}"
  customer_gateway_ip_address = "189.39.55.66"
  destination_cidr_blocks     = [
    "192.168.0.0/16"
  ]
  destination_cidr_block_count = 1
}

output "output" {
  value = {
    customer_gateway     = "${module.test.customer_gateway}"
    vpn_connection       = "${module.test.vpn_connection}"
    vpn_gateway          = "${module.test.vpn_gateway}"
    vpn_connection_route = "${module.test.vpn_connection_route}"
  }
}

module "test_disabled" {
  source  = "../"
  account = "${var.account}"

  vpn_name                    = "vpc-acme-disabled"
  customer_name               = "customer-acme-disabled"
  vpc_id                      = "${aws_vpc.test.id}"
  vpn_gateway_id              = "${lookup(module.test.vpn_gateway, "id")}" 
  customer_gateway_ip_address = "189.39.55.67"
  destination_cidr_blocks     = [
    "192.168.0.0/16"
  ]
  destination_cidr_block_count = 1
  enable = false
}
