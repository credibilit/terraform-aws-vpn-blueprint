variable "account" {
  description = "The AWS account number"
}

variable "vpc_id" {
  description = "The VPC ID to create in"
}

variable "customer_gateway_bgp_asn" {
  description = "The gateway's Border Gateway Protocol (BGP) Autonomous System Number (ASN). Default: 65000"
  default = 65000
}

variable "customer_gateway_ip_address" {
  description = "The IP address of the gateway's Internet-routable external interface."
}

variable "vpn_name" {
  description = "Unique name for VPN"
}

variable "customer_name" {
  description = "Unique name for Customer Gateway/Connection"
}

variable "static_routes_only" {
  description = "Whether the VPN connection uses static routes exclusively. Static routes must be used for devices that don't support BGP. Default: true"
  default = "true"
}

variable "destination_cidr_blocks" {
  type = "list"
  description = "List of CIDR blocks associated with the local subnet of the customer network"
}

variable "destination_cidr_block_count" {
  description = "Number of destination_cidr_blocks"
}

variable "enable" {
  default = "true"
}

variable "vpn_gateway_id" {
  default = ""
}

variable "customer_gateway_count" {
  default = 0
}

variable "vpn_conn_count" {
  default = 0
}
