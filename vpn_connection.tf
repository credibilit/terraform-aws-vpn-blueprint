// VPN Connection
resource "aws_vpn_connection" "vpn_conn" {
  vpn_gateway_id      = "${aws_vpn_gateway.vpn_gateway.id}"
  customer_gateway_id = "${aws_customer_gateway.customer_gateway.id}"
  type                = "ipsec.1"
  static_routes_only  = "${var.static_routes_only}"
  tags {
    Name = "${var.customer_name}"
  }
  count = "${var.enable ? var.vpn_conn_count : 0}"
}

output "vpn_connection" {
  value = {
    id                             = "${aws_vpn_connection.vpn_conn.id}"
    customer_gateway_configuration = "${aws_vpn_connection.vpn_conn.customer_gateway_configuration}"
    customer_gateway_id            = "${aws_vpn_connection.vpn_conn.customer_gateway_id}"
    static_routes_only             = "${aws_vpn_connection.vpn_conn.static_routes_only}"
    tags                           = "${aws_vpn_connection.vpn_conn.tags}"
    tunnel1_address                = "${aws_vpn_connection.vpn_conn.tunnel1_address}"
    tunnel1_preshared_key          = "${aws_vpn_connection.vpn_conn.tunnel1_preshared_key}"
    tunnel2_address                = "${aws_vpn_connection.vpn_conn.tunnel2_address}"
    tunnel2_preshared_key          = "${aws_vpn_connection.vpn_conn.tunnel2_preshared_key}"
    type                           = "${aws_vpn_connection.vpn_conn.type}"
    vpn_gateway_id                 = "${aws_vpn_connection.vpn_conn.vpn_gateway_id}"
  }
}
