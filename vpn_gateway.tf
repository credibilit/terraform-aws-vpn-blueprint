// VPN Gateway
resource "aws_vpn_gateway" "vpn_gateway" {
  vpc_id = "${var.vpc_id}"
  tags {
    Name = "${var.vpn_name}"
  }
  count = "${var.enable ? 1 : 0}"
}

output "vpn_gateway" {
  value = {
    id = "${aws_vpn_gateway.vpn_gateway.id}"
  }
}
