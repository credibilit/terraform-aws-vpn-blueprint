// Customer Gateway
resource "aws_customer_gateway" "customer_gateway" {
  bgp_asn    = "${var.customer_gateway_bgp_asn}"
  ip_address = "${var.customer_gateway_ip_address}"
  type       = "ipsec.1"
  tags {
    Name = "${var.customer_name}"
  }
  count = "${var.enable ? var.customer_gateway_count : 0}"
}

output "customer_gateway" {
  value = {
    id         = "${aws_customer_gateway.customer_gateway.id}"
    bgp_asn    = "${aws_customer_gateway.customer_gateway.bgp_asn}"
    ip_address = "${aws_customer_gateway.customer_gateway.ip_address}"
    type       = "${aws_customer_gateway.customer_gateway.type}"
    tags       = "${aws_customer_gateway.customer_gateway.tags}"
  }
}
