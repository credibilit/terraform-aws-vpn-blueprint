// Routes
resource "aws_vpn_connection_route" "route" {
  destination_cidr_block = "${element(var.destination_cidr_blocks, count.index)}"
  vpn_connection_id      = "${aws_vpn_connection.vpn_conn.id}"

  count = "${var.enable ? var.destination_cidr_block_count : 0}"
}

output "vpn_connection_route" {
  value = {
    destination_cidr_block = "${aws_vpn_connection_route.route.destination_cidr_block}"
    vpn_connection_id      = "${aws_vpn_connection_route.route.vpn_connection_id}"
  }
}
