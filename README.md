AWS VPN Blueprint
=================

This Terraform module is a blueprint to make the basic setup of the AWS VPN to avoid manual intervention.

# Use

To configure a VPN with one Customer Gateway with this module you need to insert the following piece of code on your own module:

```
// Call the module
module "<YOUR_MODULE_NAME>" {
  source  = "git::ssh://git@bitbucket.org/credibilit/terraform-aws-vpn-blueprint.git?ref=<VERSION>"
  account = "${var.account}"

  // Required
  vpn_name                    = "${var.vpn_name}"
  customer_name               = "${var.customer_name}"
  vpc_id                      = "${aws_vpc.test.id}"
  customer_gateway_ip_address = "123.123.123.123"
  destination_cidr_blocks     = [
    "192.168.0.0/16"
  ]
  destination_cidr_block_count = 1

  // Optional
  static_routes_only       = "false"
  customer_gateway_bgp_asn = 65000
}
```

Where `<VERSION>` is the desired version of *this* module. The master branch store the list of versions which can be used. The possible parameters are listed in advance on this document.

## Input parameters

The following parameters are used on this module:

- `account`: The AWS account ID
- `vpc_id`: The VPC ID to create in
- `customer_gateway_bgp_asn`: The gateway's Border Gateway Protocol (BGP) Autonomous System Number (ASN). Default: 65000
- `customer_gateway_ip_address`: The IP address of the gateway's Internet-routable external interface
- `vpn_name`: Unique name for VPN
- `customer_name`: Unique name for Customer Gateway/Connection
- `static_routes_only`: Whether the VPN connection uses static routes exclusively. Static routes must be used for devices that don't support BGP. Default: true
- `destination_cidr_blocks`: List of CIDR blocks associated with the local subnet of the customer network
- `destination_cidr_block_count`: Number of destination_cidr_blocks

## Output parameters

This are the outputs exposed by this module.

- `customer_gateway`
- `vpn_connection`
- `vpn_gateway`
- `vpn_connection_route`
